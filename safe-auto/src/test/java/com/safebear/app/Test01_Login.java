package com.safebear.app;

import com.safebear.app.pages.WelcomePage;
import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class Test01_Login extends BaseTest {
    @Test
    public void testLogin(){
        Assert.assertTrue(true);
        //Step 1 confirm we're on the welcome page
        assertTrue(this.welcomePage.checkCorrectPage());
        //Step 2 click on the login link and the login page loads
        assertTrue(this.welcomePage.clickOnLogin(this.loginPage));
        //step 3 login with valid credentials
        assertTrue(loginPage.login(this.userPage,"testuser","testing"));
    }
}
